from django.db import models


class NewsPost(models.Model):
    class NewsPostManager(models.Manager):
        pass

    objects = NewsPostManager()
    publication_datetime = models.DateTimeField(verbose_name="дата публикации")
    header = models.CharField(max_length=256, verbose_name="заголовок публикации")
    content = models.TextField(verbose_name="текстовый контент")

    class Meta:
        app_label = "news"
