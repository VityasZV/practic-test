import logging

from ninja_extra import api_controller, http_get
from ninja_extra.pagination import PageNumberPaginationExtra, paginate
from ninja_extra.schemas import PaginatedResponseSchema

from news.models import NewsPost
from news.schemas import NewsPostOutSchema

logger = logging.getLogger(__name__)


@api_controller(prefix_or_class="/news")
class NewsController:
    @http_get(path="", response=PaginatedResponseSchema[NewsPostOutSchema])
    @paginate(PageNumberPaginationExtra)
    def get_news(self):
        logger.debug(NewsPost.objects.all().order_by("publication_datetime"))
        return NewsPost.objects.all().order_by("publication_datetime")
