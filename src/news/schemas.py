import datetime
import typing

from django.conf import settings
from ninja import Schema
from pydantic import Field

news_datetime: typing.TypeAlias = datetime.datetime


class NewsPostOutSchema(Schema):
    date: news_datetime = Field(..., alias="publication_datetime")
    subject: str = Field(..., alias="header")
    content: str = Field(..., alias="content")

    class Config(Schema.Config):
        json_encoders = {news_datetime: lambda dt: dt.strftime(settings.NEWS_DATETIME_FORMAT)}
