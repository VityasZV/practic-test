from django.conf import settings
from django.contrib import admin

from news.models import NewsPost


@admin.register(NewsPost)
class NewsPostAdmin(admin.ModelAdmin):
    list_display = ["publication_datetime_formatted", "header", "content"]

    def publication_datetime_formatted(self, obj: NewsPost):
        return obj.publication_datetime.strftime(settings.DATETIME_FORMAT)

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False
