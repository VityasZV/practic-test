from ninja_extra import NinjaExtraAPI

from medusa_light.renderer import MedusaLightJSONEncoder
from news.api import NewsController

api = NinjaExtraAPI(title="Medusa Light", renderer=MedusaLightJSONEncoder())
api.register_controllers(NewsController)
