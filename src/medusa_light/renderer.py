from django.conf import settings
from ninja.renderers import JSONRenderer
from ninja.responses import NinjaJSONEncoder

from news.schemas import news_datetime


class MyJsonRenderer(NinjaJSONEncoder):
    def default(self, o):
        if isinstance(o, (news_datetime,)):
            return o.strftime(settings.NEWS_DATETIME_FORMAT)
        return super().default(o)


class MedusaLightJSONEncoder(JSONRenderer):
    encoder_class = MyJsonRenderer
